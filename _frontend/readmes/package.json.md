
{
  "scripts": {
    "gen": "graphql-codegen --config codegen.yml"
  },
  "dependencies": {
    "@auth0/angular-jwt": "^2.0.0",
    "apollo-angular": "^1.5.0",
    "apollo-angular-link-http": "^1.3.1",
    "apollo-cache-inmemory": "^1.3.2",
    "apollo-client": "^2.4.0",
    "apollo-link": "^1.2.4",
    "apollo-link-context": "^1.0.12",
    "apollo-upload-client": "^9.1.0",
    "apollo-upload-file": "^8.0.1",
    "graphql": "^14.1.1",
    "graphql-tag": "^2.10.0"
  },
  "devDependencies": {
    "@graphql-codegen/cli": "^1.3.1",
    "@graphql-codegen/typescript": "^1.3.1",
    "@graphql-codegen/typescript-apollo-angular": "^1.3.1",
    "@graphql-codegen/typescript-operations": "^1.3.1"
  }
}
