import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** An ISO 8601-encoded datetime */
  ISO8601DateTime: any;
};




export type Mutation = {
  __typename?: 'Mutation';
  /** Login for users */
  login?: Maybe<User>;
  /** Logout for users */
  logout?: Maybe<Scalars['Boolean']>;
  /** Unlock the user account */
  resendUnlockInstructions: Scalars['Boolean'];
  /** Set the new password */
  resetPassword?: Maybe<Scalars['Boolean']>;
  /** Send password reset instructions to users email */
  sendResetPasswordInstructions?: Maybe<Scalars['Boolean']>;
  /** Sign up for users */
  signUp?: Maybe<User>;
  /** JWT token login */
  tokenLogin?: Maybe<User>;
  /** Unlock the user account */
  unlock: Scalars['Boolean'];
  /** Update user */
  updateUser?: Maybe<User>;
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationResendUnlockInstructionsArgs = {
  email: Scalars['String'];
};


export type MutationResetPasswordArgs = {
  password: Scalars['String'];
  passwordConfirmation: Scalars['String'];
  resetPasswordToken: Scalars['String'];
};


export type MutationSendResetPasswordInstructionsArgs = {
  email: Scalars['String'];
};


export type MutationSignUpArgs = {
  attributes: UserInput;
};


export type MutationUnlockArgs = {
  unlockToken: Scalars['String'];
};


export type MutationUpdateUserArgs = {
  password?: Maybe<Scalars['String']>;
  passwordConfirmation?: Maybe<Scalars['String']>;
};

export type Query = {
  __typename?: 'Query';
  /** Returns the current user */
  me?: Maybe<User>;
};

export type User = {
  __typename?: 'User';
  createdAt: Scalars['ISO8601DateTime'];
  email?: Maybe<Scalars['String']>;
  firstName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  name: Scalars['String'];
  token: Scalars['String'];
  updatedAt: Scalars['ISO8601DateTime'];
};

/** Attributes to create a user. */
export type UserInput = {
  /** Email of user */
  email: Scalars['String'];
  /** Firstname of user */
  firstName: Scalars['String'];
  /** Lastname of user */
  lastName: Scalars['String'];
  /** Password of user */
  password: Scalars['String'];
  /** Password confirmation */
  passwordConfirmation: Scalars['String'];
};

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = (
  { __typename?: 'Mutation' }
  & { login?: Maybe<(
    { __typename?: 'User' }
    & Pick<User, 'email'>
  )> }
);

export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    email
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LoginGQL extends Apollo.Mutation<LoginMutation, LoginMutationVariables> {
    document = LoginDocument;
    
  }