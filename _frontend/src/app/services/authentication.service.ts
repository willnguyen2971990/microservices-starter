import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {map} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment as env} from '../../environments/environment';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  constructor(private http: HttpClient,
              private router: Router) {
  }

  login(credentials): Observable<any> {
    return this.http.post<{ jwt: string }>(`${env.apiUrl}/tokens`, credentials)
      .pipe(
        map(result => {
          localStorage.setItem('token', result.jwt);
          return true;
        },
      ));
  }

  fetchPermissions() {
    if (this.perms === null) {
      // this.dataGQL.fetch({id: +this.id}).toPromise()
      //   .then(({data}) => {
      //     const permissions = [];
      //     data['adminUser']['permissions'].forEach(perm => { permissions.push(perm['name']); });
      //     localStorage.setItem('perms', permissions.join('_'));
      //   });
    }
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('perms');
    this.router.navigateByUrl('/logout');
  }

  verify(code: string): Observable<any> {
    return this.http.post(`${env.apiUrl}/admin-v2/verifications`, {code});
  }

  resetPassword(email: string): Observable<any> {
    return this.http.post(`${env.apiUrl}/reset-password`, {email});
  }

  updatePassword(params): Observable<any> {
    return this.http.put(`${env.apiUrl}/reset-password`, params);
  }

  get token(): string {
    return localStorage.getItem('token');
  }

  get perms(): any {
    return localStorage.getItem('perms');
  }

  get jwtHelper(): JwtHelperService {
    return new JwtHelperService();
  }

  get isLoggedIn(): boolean {
    if (this.token === null) {
      return false;
    }

    return !this.jwtHelper.isTokenExpired(this.token);
  }

  get employer() {
    if (this.token === null) {
      return '';
    }

    return this.jwtHelper.decodeToken(this.token).employer;
  }

  get id(): string {
    if (this.token === null) {
      return '';
    }

    return this.jwtHelper.decodeToken(this.token).sub;
  }

  get email(): string {
    if (this.token === null) {
      return '';
    }

    return this.jwtHelper.decodeToken(this.token).email;
  }

  get employerId(): string {
    if (this.token === null) {
      return '';
    }

    return this.jwtHelper.decodeToken(this.token).employerId;
  }

  get employerSlug(): string {
    if (this.token === null) {
      return '';
    }

    return this.jwtHelper.decodeToken(this.token).employerSlug;
  }

  get isSuperAdmin(): boolean {
    if (this.token === null) {
      return false;
    }

    return this.jwtHelper.decodeToken(this.token).roles.indexOf('super_admin') !== -1;
  }

  get isAdmin(): boolean {
    if (this.token === null) {
      return false;
    }

    return this.jwtHelper.decodeToken(this.token).roles.indexOf('admin') !== -1;
  }

  get isEmployer(): boolean {
    if (this.token === null) {
      return false;
    }

    return this.jwtHelper.decodeToken(this.token).roles.indexOf('employer') !== -1;
  }

  get isAdminOrAbove(): boolean {
    return this.isAdmin || this.isSuperAdmin;
  }

  isAuthorized(allowedRoles: string[]): boolean {
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }

    const role = this.jwtHelper.decodeToken(this.token).roles[0];
    return allowedRoles.includes(role);
  }

  hasPermission(permName: string): boolean {
    if (this.isSuperAdmin) {
      return true;
    }
    if (this.perms === null) {
      return false;
    }
    return this.perms.split('_').some(permission => permission === permName);
  }
}
